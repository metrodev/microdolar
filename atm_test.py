import unittest

import atm

class ATMTest(unittest.TestCase):
  def test_is_number(self):
    rate = atm.get_atm()
    return self.assertTrue(type(rate) is int or type(rate) is float)