import requests

def get_atm():
  response = requests.get('https://rates.airtm.com/rates.json')

  data = response.json()

  day = data['today']
  key = 'VES_BANK'
  final = None

  if key not in day:
    day = data['yesterday']

  assert day[key]['rate'] is not None

  return day[key]['rate']