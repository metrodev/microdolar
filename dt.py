import requests

def get_dt():
  res = requests.get('https://s3.amazonaws.com/dolartoday/data.json')

  data = res.json()

  return data['USD']['dolartoday']