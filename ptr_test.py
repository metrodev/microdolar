import unittest
import ptr

class PtrTest(unittest.TestCase):
  def test_is_number(self):
    self.assertIsInstance(ptr.get_ptr(), float)