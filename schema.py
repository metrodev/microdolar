"""
graphql schema for our api
"""
import graphene
from graphene import ObjectType, ID, String, Float, List, Field, Int

from persistence import get_rates, get_sources

class Rate(ObjectType):
  id = ID()
  date = String(description='Date when the rate was fetched')
  symbol = String(description='Symbol of the currency being tracked')
  price = Float(description='Price of the currency being tracked')
  currency = String(description='The currency in which the price is expressed')
  source = String(description='The source of the data')

class Query(ObjectType):
  rates = List(Rate, from_id=ID(), source=String(), limit=Int())
  sources = List(String)

  def resolve_rates(query, info, from_id=None, source=None, limit=50):
    return get_rates(limit, from_id=from_id, source=source)

  def resolve_sources(query, info):
    return get_sources()