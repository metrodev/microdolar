import requests
from scrap import scrap

def get_bcv():
  dom = scrap('http://www.bcv.org.ve/')

  raw =  dom.find(id='dolar').strong.text.strip()

  raw = raw.replace('.', '').replace(',', '.')

  return float(raw)