import requests

def get_ptr():
  """
  get the price of a petro in VES
  """
  response = requests.post('https://petroapp-price.petro.gob.ve/price/', json={
    'coins': ['PTR'],
    'fiats': ['BS']
  })

  data = response.json()['data']

  return data['PTR']['BS']