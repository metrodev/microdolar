import unittest
from starlette.applications import Starlette

import server

class ServerTest(unittest.TestCase):
  def test_server_sanity(self):
    self.assertIsInstance(server.app, Starlette)

