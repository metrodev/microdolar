FROM python:alpine

WORKDIR /usr/app
ENV SQLITE_DB /storage/sqlite3.db

COPY requirements.txt /usr/app

RUN apk add --no-cache --virtual .build-deps gcc musl-dev make libxml2-dev libxslt-dev \
 && apk add --no-cache libxslt libxml2 \
 && pip install -r requirements.txt \
 && apk del .build-deps

COPY . /usr/app

