from datetime import datetime

import btc
import bcv
import dt
import atm

import persistence

sources = {
  'btc': btc.get_btc_usd,
  'bcv': bcv.get_bcv,
  'dt': dt.get_dt,
  'atm': atm.get_atm,
}

def load_data():
  rates = {}

  for src in sources:
    try:
      rates[src] = sources[src]()
    except:
      rates[src] = None

  return rates

def persist_data():
  data = load_data()
  now = datetime.utcnow()

  for src in data:
    if data[src]:
      persistence.add_rate(now.isoformat(), 'USD', data[src], 'VES', src)
