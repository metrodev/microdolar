import unittest
import bcv

class BcvTest(unittest.TestCase):
  def test_is_number(self):
    self.assertIsInstance(bcv.get_bcv(), float)