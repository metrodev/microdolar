import requests

def currency(name, data):
  """
  returns the price of 1 bitcoin
  """
  return float(data[name]['rates']['last'])

def get_btc():
  """
  Get the price of a bitcoin in VES 
  using the BTC prices as reference
  """
  response = requests.get('https://localbitcoins.com/bitcoinaverage/ticker-all-currencies/')

  data = response.json()

  ves = currency('VES', data)

  return ves

def get_btc_usd():
  """
  Get the price of a dollar in VES 
  using the BTC prices as reference
  """
  response = requests.get('https://localbitcoins.com/bitcoinaverage/ticker-all-currencies/')

  data = response.json()

  usd = currency('USD', data)
  ves = currency('VES', data)

  return ves / usd