import unittest
import dt

class DtTest(unittest.TestCase):
  def test_is_number(self):
    self.assertIsInstance(dt.get_dt(), float)