import data
import time

if __name__ == "__main__":
  print("Starting data worker...")

  while True:
    data.persist_data()
    print("Sleeping 14 minutes")
    time.sleep(60*14)