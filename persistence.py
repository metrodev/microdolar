import sqlite3
import os
import os.path

"""
SQLITE_DB should be the absolute path to the db
"""
DB_PATH = os.environ.get('SQLITE_DB', './database.db')

"""
db instance
"""
tables_created = False

def get_db():
  db = sqlite3.connect(DB_PATH)
  if not tables_created:
    create_tables(db)
  return db

def create_tables(db):
  c = db.cursor()

  c.execute("""
  CREATE TABLE IF NOT EXISTS rates
             (date text, symbol text, price real, currency text, source text)
  """)

  db.commit()

"""
date - date when the rate was fetched
symbol - The symbol of the currency being tracked e.g. (USD, BTC)
price - The price of the currency
currency - The currency in which the price is expressed
source - Some label for the source of the rate
"""
def add_rate(date, symbol, price, currency, source):
  db = get_db()
  c = db.cursor()
  
  c.execute("""
    INSERT INTO rates (date, symbol, price, currency, source)
    VALUES
    (datetime(?), ?, ?, ?, ?)
  """, (date, symbol, price, currency, source))

  db.commit()

def get_rates(limit=50, from_id=None, **params):
  with get_db() as db:
    c = db.cursor()
    fields = ('id', 'date', 'symbol', 'price', 'currency', 'source')
    query = f"SELECT rowid, (date || ' UTC') as date, symbol, price, currency, source FROM rates WHERE 1=1"
    query_params = []

    if from_id:
      query += f" AND rowid < ?"
      query_params.append(from_id)

    for k in params:
      if k in fields and params[k] is not None:
        if k == 'id':
          k = 'rowid'
        
        query += f" AND {k} = ?"
        query_params.append(params[k])

    query += f" ORDER BY rowid DESC LIMIT {int(max(limit, 500))}"

    c.execute(query, query_params)

    return [ dict(zip(fields, row)) for row in c.fetchall() ]

def get_sources():
  with get_db() as db:
    c = db.cursor()
    c.execute('SELECT source FROM rates GROUP BY source')

    return [row[0] for row in c.fetchall()]