import requests
from bs4 import BeautifulSoup

def scrap(url):
  response = requests.get(url)

  return BeautifulSoup(response.text, 'lxml')