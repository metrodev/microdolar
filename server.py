from starlette.applications import Starlette
from starlette.routing import Route
from starlette.responses import JSONResponse
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware

from starlette.graphql import GraphQLApp
import graphene

import schema
import persistence

async def rates(request):
  return JSONResponse(persistence.get_rates())

routes = [
  Route('/rates', rates),
  Route('/graphql', GraphQLApp(schema=graphene.Schema(query=schema.Query)))
]

middleware = [
  Middleware(CORSMiddleware, allow_origins=['*'], allow_methods=['GET', 'OPTIONS', 'POST'], allow_headers=['*'])
]

app = Starlette(False, routes, middleware=middleware)